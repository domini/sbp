var app = (function(document, $) {

	'use strict';
	var docElem = document.documentElement,

		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		_init = function() {
			$(document).foundation();
			_userAgentInit();

			window.globals = {
				controller: window.location.pathname,
				nav_stick_on: parseInt( $('#mainNav').offset().top + $('#mainNav').outerHeight() )
			}

			$('a[data-dropdown]').hover(function() {
				$(this).megaMenu();
			}, function() {
				var isHovered = $( '#' + $(this).attr('data-dropdown') ).is(':hover');
				if (!isHovered) { $(this).megaMenu(); }
			});

			$(window).scroll(function(event) {
				$('#mainNav').stickNav({target: $('section[role="fixed-navigation"]')}, function(settings){
					$('section[role="dropdown-container"]').stickNav({stickTo: 70})
				});
			});

			if (globals.controller !== '/') {
				$('header').addClass('after-border');
			};

			if (globals.controller.indexOf('seminario-submeter') != -1){
				$.fn.tinyCall();				
			};

			$('.image-uploaded a').on('click', function(event) {
				event.preventDefault();
				$(this).toggleClass('active');

			});
			$('.show-images').on('click', function(event) {
				event.preventDefault();
				$('.clearing-thumbs').find('a').eq(0).trigger('click');
			});
		};

	return {
		init: _init
	};

})(document, jQuery);

var utils = (function(document, $) {

	'use strict';
	$.fn.megaMenu = function(options, callback) {
		var config = $.extend(true, {
			emitter: this,
			target: null || $( '#' + this.attr('data-dropdown') ),
		}, options);

		if ( typeof config.target === 'object' ) {
			if(config.target.hasClass('open')) { return false; }
			config.target.fadeToggle('fast', function() {
				$(this).mouseleave(function(event) {
					$(this).fadeOut('fast');
				});
			});
		}

		if (typeof callback === 'function') {
			callback.call(this, config);
		}

		return false;
	};

	$.fn.stickNav = function(options, callback) {
		var settings = $.extend(true, {
			stickTo: 0,
			isSticked: false,
			top: 0 || globals.nav_stick_on,
			target: null,
		}, options);

		var css = {
			zIndex: 100,
			position: 'fixed',
			display: 'block',
			'backface-visibility': 'hidden',
			'box-shadow': 'rgba(0,0,0,.3) 0 10px 12px -8px',
			top: settings.stickTo,
			left: 0,
			right: 0
		}

		if ( $(window).scrollTop() >= settings.top ) {

			if (settings.target !== null) {
				settings.target.css( css );
			} else {
				this.css( css );
			}

		} else {
			if (settings.target !== null) {
				settings.target.removeAttr('style');
			} else {
				this.removeAttr('style');
			}
		}

		if (typeof callback == 'function') {
			callback.call(this, settings);
		};
	};
	$.fn.tinyCall = function(){
		var tinyIds = ['historiaClinicaEdit', 'diagnosticoEdit', 'handoutEdit', 'bibliografiaEdit'];
		$.each(tinyIds, function(key, value) {
			var editor = new TINY.editor.edit(value, {
				id: value,
				width: 672,
				height: 200,
				cssclass: 'tinyeditor',
				controlclass: 'tinyeditor-control',
				rowclass: 'tinyeditor-header',
				dividerclass: 'tinyeditor-divider',
				controls: [
					'orderedlist', 
					'unorderedlist', 
					'|',
					'leftalign',
					'centeralign', 
					'rightalign', 
					'blockjustify', 
					'|', 
					'bold', 
					'italic', 
					'underline', 
					'strikethrough', 
					'|', 
					'subscript', 
					'superscript', 
					'|', 
					'outdent', 
					'indent', 
					'|',
					'size'
					],
				footer: false,
				xhtml: true,
				bodyid: 'editor',
				footerclass: 'tinyeditor-footer',
				resize: {cssclass: 'resize'}
			});
		});				
	};

})(document, jQuery);

(function() {

	'use strict';
	app.init();

})();